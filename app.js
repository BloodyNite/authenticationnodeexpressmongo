const moongose = require("mongoose")
const express = require("express")
const app = express()

const bodyParse = require("body-parser")
const cookieParse= require("cookie-parser")
const cors = require("cors")
require("dotenv").config()
//DB Connection
moongose.connect(process.env.DATABASE,{
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true
}).then(()=>{
    console.log("DB CONNECTED")
}).catch(()=>{
    console.log("UNABLE to connect to DB")
})

// Use parsing middleware
app.use(bodyParse.json())
app.use(cookieParse())
app.use(cors())

//Import routes
const userRoutes = require("./routes/user")

//Using routes
app.use('/api', userRoutes) //localhost:8000/api/signup

const port = process.env.PORT || 8000

//Starting a server
app.listen(port, ()=>{
    console.log(`App is running at ${port}`)
})